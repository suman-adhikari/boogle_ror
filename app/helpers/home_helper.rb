module HomeHelper


    def generateCode(number)
        charset = Array('a'..'z')
        return Array.new(number) { charset.sample }.join
    end

     def generateBoardArray(randomString)
        @randomString = randomString 
        board = Array.new(4) { Array.new(4)}      
        x=0;
        y=0;
        count =0
        for x in 0..3 do
            for y in 0..3 do
                board[x][y] = @randomString[count]             
                count +=1
            end     
        end
        return board
    end

    def validateAgainstDict(word)   
            words = {}
            file = File.open("./public/sampleWords.txt")
            file.each do |line|
                if word == line.strip
                  return true
                end
            end      
        return false
      end
   

## class start
class Boggle
         
  def initialize (randomString)
    @board = {}
    @AdjNodes = {}
    generateBoard(randomString)
    getAdjacentNodes
  end

    def generateBoard(randomString)
      randomString.split("").each_with_index do |letter, index|
      @board[index.divmod(4)] = letter
      end
    end

   def getAdjacentNodes
     @board.each do |coord, letter|
      xnode = [coord[0], coord[0]+1, coord[0]-1].select { |x| x >= 0 && x <=3 }
      ynode = [coord[1], coord[1]+1, coord[1]-1].select { |x| x >= 0 && x <=3 }
        
      arr = []
      counter=-1

      for i in xnode do
        for j in ynode do
          counter += 1
          if(counter!=0) 
            arr.push([i,j])
          end
        end
      end

      @AdjNodes[coord] = arr
    end
end

  def validateWord(word)
    word.strip
    @board.each do |coord, letter|
      if letter == word[0]
        lettersToValidate = word[1..-1]
        return true if isNextNodeValid(lettersToValidate,coord)
      end
    end
    false
  end

  def isNextNodeValid(lettersToValidate, coord, history = [])
    return true if lettersToValidate.empty?
    currentLetter = lettersToValidate[0]
    lettersToValidate = lettersToValidate[1..-1] 

    @AdjNodes[coord].each do |adjNode|
      unless history.include?(adjNode) || @board[adjNode] != currentLetter
        if isNextNodeValid(lettersToValidate, adjNode, history + [coord])
          return true
        end
      end
    end
    false
  end
 
end
 
## class end

end
