import configureMockStore from "redux-mock-store";
import {
  startTimer,
  resetTimer,
  ResetUserWords
} from "../../javascript/redux/actions/timerAction";

const mockStore = configureMockStore();
const store = mockStore({});

describe("action creators", () => {
  it("creates START_TIMER when start button is clicked", () => {
    store.dispatch(startTimer());
    expect(store.getActions()).toMatchSnapshot();
  });

  it("creates RESET_TIMER when start button is clicked", () => {
    store.dispatch(resetTimer());
    expect(store.getActions()).toMatchSnapshot();
  });

  it("creates RESET_WORD when start button is clicked", () => {
    store.dispatch(ResetUserWords());
    expect(store.getActions()).toMatchSnapshot();
  });
});
