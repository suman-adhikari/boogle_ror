import configureMockStore from "redux-mock-store";
import {
  AddUserWords,
  GetBoardData,
  GetBoard,
  SendWordForValidation
} from "../../javascript/redux/actions/boggleAction";
import * as type from "../../javascript/redux/actions/actionTypes";
import thunk from "redux-thunk";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore();
const mockAxios = jest.genMockFromModule("axios");

const mockBoardData = [
  ["i", "d", "v", "d"],
  ["t", "f", "t", "s"],
  ["f", "o", "c", "j"],
  ["q", "j", "k", "q"]
];

const mockData = "Ram";

describe("action creators", () => {
  it("creates GET_BOARD", async () => {
    mockAxios.get.mockImplementationOnce(() => {
      Promise.resolve({ data: mockBoardData });
    });

    const expectedActions = { type: type.GET_BOARD, boardData: mockBoardData };

    store
      .dispatch(GetBoard())
      .then(() => expect(store.getActions()).toEqual(expectedActions));
  });

  it("creates ADD_WORD", async () => {
    mockAxios.post.mockImplementationOnce(() => {
      Promise.resolve({ data: mockData });
    });

    let submittedWords = { word: mockData, valid: true };
    const expectedActions = {
      type: type.GET_BOARD,
      submittedWords: submittedWords
    };

    store
      .dispatch(SendWordForValidation(mockData))
      .then(() => expect(store.getActions()).toEqual(expectedActions));
  });

  it("creates ADD_WORDS", () => {
    store.dispatch(AddUserWords());
    expect(store.getActions()).toMatchSnapshot();
  });

  it("creates GET_BOARD", () => {
    store.dispatch(GetBoardData());
    expect(store.getActions()).toMatchSnapshot();
  });
});
