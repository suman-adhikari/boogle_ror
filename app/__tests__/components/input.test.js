import React from "react";
import Enzyme, { shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import {
  Input,
  mapStateToProps,
  mapDispatchToProps
} from "../../javascript/components/Input";
import configureMockStore from "redux-mock-store";

Enzyme.configure({ adapter: new Adapter() });

function setup() {
  const props = {
    submittedWords: [],
    actions: {},
    submitButton: true,
    handleSubmit: jest.fn(),
    onChange: jest.fn(),
    word: "hy"
  };

  const enzymeWrapper = mount(<Input {...props} />);

  return {
    props,
    enzymeWrapper
  };
}

describe("<Input> component", () => {
  it("should render form", () => {
    const { enzymeWrapper, props } = setup();
    expect(enzymeWrapper.find("form")).toHaveLength(1);
    expect(enzymeWrapper.find("button")).toHaveLength(1);
  });

  it("should trigger onChange event", () => {
    const { enzymeWrapper } = setup();
    const input = enzymeWrapper.find("input");
    input.instance().value = "hello";
    input.simulate("change");
    expect(enzymeWrapper.state().word).toEqual("hello");
  });

  it("test mapStateToProps", () => {
    const initialState = {
      submittedWords: ["one", "two"]
    };
    expect(mapStateToProps(initialState).submittedWords).toEqual([
      "one",
      "two"
    ]);
  });
});
