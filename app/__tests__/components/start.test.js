import React from "react";
import { shallow } from "enzyme";
import Start from "../../javascript/components/Start";

function setup() {
  const props = {
    handleClick: jest.fn(),
    displayStart: true
  };

  const enzymeWrapper = shallow(<Start {...props} />);

  return {
    props,
    enzymeWrapper
  };
}

describe("<Start>", () => {
  it("should render and check button and its text", () => {
    const { enzymeWrapper } = setup();
    const btn = enzymeWrapper.find("button");
    expect(btn.hasClass("btn")).toBe(true);
    expect(btn.text()).toEqual("Start");
  });

  it("simulate start btn click", () => {
    const { props, enzymeWrapper } = setup();
    const btn = enzymeWrapper.find("button");
    expect(btn.hasClass("btn")).toBe(true);
    btn.simulate("click");
    expect(props.handleClick.mock.calls.length).toBe(1); // checking if the mock handleClick was called
  });
});
