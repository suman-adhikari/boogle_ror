import React from "react";
import { shallow } from "enzyme";
import { Score } from "../../javascript/components/Score";

function setup() {
  const props = {
    submittedWords: [
      { word: "and", valid: true },
      { word: "test", valid: false },
      { word: "Noice", valid: true }
    ]
  };

  const enzymeWrapper = shallow(<Score {...props} />);

  return {
    props,
    enzymeWrapper
  };
}

describe("<Score>", () => {
  it("should render score with correct score points", () => {
    const { enzymeWrapper, props } = setup();
    const h1 = enzymeWrapper.find("h1");
    expect(h1).toHaveLength(1);

    let score = 0;
    props.submittedWords
      .filter(s => s.valid)
      .forEach(item => {
        score += item.word.length;
      });
    expect(h1.text()).toEqual("Score: " + score);
  });
});
