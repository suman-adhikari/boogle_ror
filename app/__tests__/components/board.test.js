import React from "react";
import { shallow } from "enzyme";
import { Board } from "../../javascript/components/Board";

function setup() {
  const props = {
    boardData: [
      ["i", "d", "v", "d"],
      ["t", "f", "t", "s"],
      ["f", "o", "c", "j"],
      ["q", "j", "k", "q"]
    ],
    actions: {}
  };

  const enzymeWrapper = shallow(<Board {...props} />);

  return {
    props,
    enzymeWrapper
  };
}

describe("<Board>", () => {
  it("should render board", () => {
    const { enzymeWrapper, props } = setup();
    const table = enzymeWrapper.find("table");
    expect(table).toHaveLength(1);
    expect(table.hasClass("table-bordered")).toEqual(true);
    expect(table.find("tr")).toHaveLength(props.boardData.length);
  });
});
