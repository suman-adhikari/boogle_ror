import React from "react";
import { shallow } from "enzyme";
import { UserWords } from "../../javascript/components/UserWords";

function setup() {
  const props = {
    submittedWords: [
      { word: "test1", valid: true },
      { word: "test2", valid: false },
      { word: "test3", valid: false }
    ]
  };

  const enzymeWrapper = shallow(<UserWords {...props} />);

  return {
    props,
    enzymeWrapper
  };
}

describe("UserWords component", () => {
  it("should render all User Words", () => {
    const { enzymeWrapper, props } = setup();
    const div = enzymeWrapper.find("div");
    expect(div).toHaveLength(1);
    const span = enzymeWrapper.find("span");
    expect(span).toHaveLength(props.submittedWords.length);
  });
});
