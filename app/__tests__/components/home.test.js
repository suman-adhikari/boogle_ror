import React from "react";
import Enzyme, { shallow } from "enzyme";
import { Home } from "../../javascript/components/Home";
import { Timer } from "../../javascript/components/Timer";
import configureMockStore from "redux-mock-store";
import Adapter from "enzyme-adapter-react-16";

function setup() {
  const props = {
    boardData: [
      ["i", "d", "v", "d"],
      ["t", "f", "t", "s"],
      ["f", "o", "c", "j"],
      ["q", "j", "k", "q"]
    ],
    actions: {},
    timer: 180
  };

  const enzymeWrapper = shallow(<Home {...props} />);

  return {
    props,
    enzymeWrapper
  };
}
let store;

describe("<Home>", () => {
  beforeEach(() => {
    const mockStore = configureMockStore();

    store = mockStore({
      timer: 30
    });
  });

  it("should render the child component wrapper div", () => {
    const { enzymeWrapper, props } = setup();
    expect(enzymeWrapper.find("div")).toHaveLength(1);
  });

  it("should render child component Timer", () => {
    const { enzymeWrapper, props } = setup();
    debugger;
    var x = enzymeWrapper;
    console.log(x);
  });
});
