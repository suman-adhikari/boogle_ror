import React from "react";
import { shallow } from "enzyme";
import { Timer } from "../../javascript/components/Timer";

function setup() {
  const props = {
    timer: 70
  };

  const enzymeWrapper = shallow(<Timer {...props} />);

  return {
    props,
    enzymeWrapper
  };
}

describe("<Timer>", () => {
  it("should render timer", () => {
    const { enzymeWrapper, props } = setup();
    const h1 = enzymeWrapper.find("h1");
    expect(h1).toHaveLength(1);
    expect(h1.text()).toEqual(
      "Time Remaining " +
        Math.floor(props.timer / 60) +
        ":" +
        (props.timer % 60)
    );
    expect(props.timer).toBe(70);
  });
});
