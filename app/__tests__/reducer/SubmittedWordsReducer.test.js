import SubmittedWordsReducer from "../../javascript/redux/reducers/SubmittedWordsReducer";
import * as type from "../../javascript/redux/actions/actionTypes";

let data = ["one", "two"];

describe("SubmittedWordsReducer", () => {
  it("returns initial state from reducer", () => {
    expect(SubmittedWordsReducer(undefined, {})).toEqual([]);
  });

  it("returns ADD_WORDS", () => {
    expect(
      SubmittedWordsReducer(data, {
        type: "ADD_WORDS",
        submittedWords: data
      })
    ).toEqual(data);
  });

  it("returns RESET_WORDS ", () => {
    expect(SubmittedWordsReducer(undefined, { type: "RESET_WORDS" })).toEqual(
      []
    );
  });
});
