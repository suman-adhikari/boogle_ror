import boardReducer from "../../javascript/redux/reducers/boardReducer";
import * as type from "../../javascript/redux/actions/actionTypes";

let data = [
  ["i", "d", "v", "d"],
  ["t", "f", "t", "s"],
  ["f", "o", "c", "j"],
  ["q", "j", "k", "q"]
];

describe("board_reducer", () => {
  it("should return initial state", () => {
    expect(boardReducer(undefined, {})).toMatchSnapshot();
  });

  it("should handle GET_BOARD", () => {
    expect(
      boardReducer(undefined, { type: type.GET_BOARD, boardData: data })
    ).toEqual(data);
  });
});
