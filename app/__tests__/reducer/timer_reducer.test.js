import timerReducer from "../../javascript/redux/reducers/timerReducer";
import * as type from "../../javascript/redux/actions/actionTypes";

let initialState = 60;

describe("timer_reducer", () => {
  //it("should return initial state with snapshot", () => {
  //  expect(timerReducer(undefined, {})).toMatchSnapShot();
  //});

  it("returns initial state from reducer", () => {
    expect(timerReducer(undefined, {})).toEqual(180);
  });

  it("returns initial state from test ", () => {
    expect(timerReducer(initialState, {})).toEqual(60);
  });

  it("returns START_TIMER ", () => {
    expect(timerReducer(initialState, { type: "START_TIMER" })).toEqual(59);
  });

  it("returns RESET_TIMER ", () => {
    expect(timerReducer(undefined, { type: "RESET_TIMER" })).toEqual(180);
  });
});
