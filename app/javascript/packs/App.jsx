import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import Home from "../components/Home";
import configureStore from "../redux/configureStore";
import { Provider as ReduxProvider } from "react-redux";
import CSFRhelper from "../../javascript/utils/CSFRhelper";
CSFRhelper();

const store = configureStore();
document.addEventListener("DOMContentLoaded", () => {
  ReactDOM.render(
    <ReduxProvider store={store}>
      <Home />
    </ReduxProvider>,
    document.getElementById("root")
  );
});
