import * as type from "../actions/actionTypes";

export default function boardReducer(state = [], action) {
  switch (action.type) {
    case type.GET_BOARD:
      return action.boardData;
    default:
      return state;
  }
}
