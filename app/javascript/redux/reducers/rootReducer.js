import { combineReducers } from "redux";
import submittedWords from "./SubmittedWordsReducer";
import boardData from "./boardReducer";
import timer from "./timerReducer";

const rootReducer = combineReducers({
  submittedWords,
  boardData,
  timer
});

export default rootReducer;
