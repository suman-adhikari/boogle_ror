import * as type from "../actions/actionTypes";

const initialState = [];
export default function SubmittedWordsReducer(state = initialState, action) {
  switch (action.type) {
    case type.ADD_WORD:
      return [...state, { ...action.submittedWords }];
    case type.RESET_WORDS:
      return initialState;
    default:
      return state;
  }
}
