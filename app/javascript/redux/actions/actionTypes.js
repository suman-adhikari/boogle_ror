export const ADD_WORD = "ADD_WORD";
export const RESET_WORDS = "RESET_WORDS";
export const GET_BOARD = "GET_BOARD";
export const START_TIMER = "START_TIMER";
export const RESET_TIMER = "RESET_TIMER";
