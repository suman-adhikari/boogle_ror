import * as type from "../actions/actionTypes";

export function startTimer() {
  return { type: type.START_TIMER };
}

export function resetTimer() {
  return { type: type.RESET_TIMER };
}

export function ResetUserWords() {
  return { type: type.RESET_WORDS };
}
