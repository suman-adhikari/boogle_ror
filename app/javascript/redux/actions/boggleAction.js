//this is called action creator coz it creats action called "CREATE_COURSE"
//action creator must have 'type' in return statement

import * as type from "./actionTypes";
import axios from "axios";

export const SendWordForValidation = word => {
  return dispatch => {
    return axios
      .post("/checkword", {
        data: word
      })
      .then(response => {
        let submittedWords = { word: word, valid: response.data };
        dispatch(AddUserWords(submittedWords));
      });
  };
};

export const GetBoard = () => {
  return dispatch => {
    return axios.get("/getboard.json", {}).then(response => {
      dispatch(GetBoardData(response.data));
    });
  };
};

export function AddUserWords(submittedWords) {
  return { type: type.ADD_WORD, submittedWords };
}

export function GetBoardData(boardData) {
  return { type: type.GET_BOARD, boardData };
}
