import React from "react";
import { connect } from "react-redux";

export const UserWords = ({ submittedWords }) => {
  let words = submittedWords.map((item, index) => {
    let valid = item.valid ? "badge badge-success" : "badge badge-danger";
    return (
      <span className={valid} key={item.word + index}>
        {item.word}
      </span>
    );
  });
  return <div className="userword">{words}</div>;
};

const mapStateToProps = state => ({
  submittedWords: state.submittedWords
});

export default connect(mapStateToProps)(UserWords);
