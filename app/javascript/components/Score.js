import React from "react";
import { connect } from "react-redux";

export const Score = ({ submittedWords }) => {
  let score = 0;
  submittedWords
    .filter(s => s.valid)
    .forEach(item => {
      score += item.word.length;
    });
  return <h1>Score: {score}</h1>;
};

const mapStateToProps = state => ({
  submittedWords: state.submittedWords
});

export default connect(mapStateToProps)(Score);
