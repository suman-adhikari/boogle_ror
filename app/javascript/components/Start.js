import React from "react";

const Start = props => {
  return props.displayStart ? (
    <div>
      <button
        className="btn btn-lg btn-primary"
        type="button"
        onClick={props.handleClick}
      >
        Start
      </button>
    </div>
  ) : (
    false
  );
};

export default Start;
