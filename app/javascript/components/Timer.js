import React from "react";
import { connect } from "react-redux";

export const Timer = ({ timer }) => {
  let sec = timer % 60;
  let min = Math.floor(timer / 60);

  return (
    <h1>
      Time Remaining {min}:{sec}
    </h1>
  );
};

const mapStateToProps = state => ({
  timer: state.timer
});

export default connect(mapStateToProps)(Timer);
