import React, { Component } from "react";
import { connect } from "react-redux";
import * as boggleAction from "../redux/actions/boggleAction";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";

export class Input extends Component {
  constructor(props) {
    super(props);
    this.state = {
      word: ""
    };
  }

  handleChange = e => {
    this.setState({ word: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ word: "" });
    this.props.actions.SendWordForValidation(this.state.word);
  };

  render() {
    let inputValue = this.state.word;
    return this.props.submitButton ? (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input
            value={inputValue}
            type="text"
            name="userword"
            onChange={this.handleChange}
            required
          ></input>
          <button className="btn btn-primary" type="submit">
            Submit
          </button>
        </form>
      </div>
    ) : (
      false
    );
  }
}

Input.propTypes = {
  actions: PropTypes.object.isRequired,
  submittedWords: PropTypes.array.isRequired
};

export function mapStateToProps(state) {
  return {
    submittedWords: state.submittedWords
  };
}

export function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(boggleAction, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Input);
