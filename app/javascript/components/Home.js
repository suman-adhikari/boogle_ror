import React, { Component } from "react";
import { connect } from "react-redux";
import Input from "./Input";
import Board from "./Board";
import UserWords from "./UserWords";
import Score from "./Score";
import Start from "./Start";
import Timer from "./Timer";
import * as timerAction from "../redux/actions/timerAction";
import * as boggleAction from "../redux/actions/boggleAction";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";

export class Home extends Component {
  state = {
    displayStart: true,
    submitButton: false
  };

  startTimer = () => {
    this.props.actions.timerActions.resetTimer();
    this.props.actions.timerActions.ResetUserWords();
    this.props.actions.boggleAction.GetBoard();

    this.setState({ displayStart: !this.state.displayStart });
    this.setState({ submitButton: !this.state.submitButton });

    let startTime = setInterval(() => {
      if (this.props.timer == 0) {
        this.setState({ displayStart: !this.state.displayStart });
        this.setState({ submitButton: !this.state.submitButton });
        clearInterval(startTime);
      } else {
        this.props.actions.timerActions.startTimer();
      }
    }, 1000);
  };

  render() {
    let displayStart = this.state.displayStart;
    let submitButton = this.state.submitButton;
    return (
      <div>
        <Timer />
        <Score />
        <Board />
        <Input submitButton={submitButton} />
        <UserWords />
        <Start displayStart={displayStart} handleClick={this.startTimer} />
      </div>
    );
  }
}

Home.propTypes = {
  actions: PropTypes.object.isRequired,
  timer: PropTypes.number.isRequired
};

function mapStateToProps(state) {
  return {
    timer: state.timer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      boggleAction: bindActionCreators(boggleAction, dispatch),
      timerActions: bindActionCreators(timerAction, dispatch)
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
