import React, { Component } from "react";
import { connect } from "react-redux";
import * as boggleAction from "../redux/actions/boggleAction";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";

export class Board extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.actions.GetBoard();
  }

  render() {
    let data = this.props.boardData;
    const tableData = data.map((x, i) => {
      return (
        <tr key={i + x}>
          {x.map((y, j) => {
            return <td key={j + x}>{y}</td>;
          })}
        </tr>
      );
    });

    return (
      <div id="table-container">
        <table
          id="board"
          className="table table-bordered table-striped text-center"
        >
          <tbody>{tableData}</tbody>
        </table>
      </div>
    );
  }
}

Board.propTypes = {
  actions: PropTypes.object.isRequired,
  boardData: PropTypes.array.isRequired
};

function mapStateToProps(state) {
  return {
    boardData: state.boardData
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(boggleAction, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Board);
