class HomeController < ApplicationController
include HomeHelper

    def index
    end

    def getBoard
        $randomstring = generateCode(16)
        @arr = generateBoardArray($randomstring)  
        render json: @arr      
    end

    def checkWord 
        word = params[:data]
        game = Boggle.new($randomstring)
        result = game.validateWord(word)  
        if result
            result = validateAgainstDict(word)
        end
        result = result && word.length>1
        render json: result  
    end

end
