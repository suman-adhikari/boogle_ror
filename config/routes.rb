Rails.application.routes.draw do
  root 'home#index'
  #resources 'home'
  get "getboard", to: "home#getBoard"
  post "checkword", to: "home#checkWord"
end
