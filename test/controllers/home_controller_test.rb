require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test "index action should be success" do
    get root_path
    assert_response :success
  end

  test "get request to /getboard should generate array of size 4" do
    get getboard_path
    json_response = JSON.parse(response.body)
    assert_equal json_response.length, 4
  end


  test "post request to /checkword should return boolean" do
    post checkword_path, params: {data:"test"}
    json_response = JSON.parse(response.body)
    assert([true, false].include? json_response)
  end

end
